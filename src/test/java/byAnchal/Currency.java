package byAnchal;

import java.util.concurrent.TimeUnit;

import org.apache.xmlbeans.impl.xb.xsdschema.ListDocument.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Currency {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		driver.get("https://nseIndia.com");
		System.out.println("Home Page Title is : "+ driver.getTitle());
		
		//Click on Derivatives 
		driver.findElement(By.xpath("//*[@id='tabs']/div/div/div/nav/div/div/a[2]")).click();
		//click on Equity Derivatives
		driver.findElement(By.xpath("//*[@id='nse-derivatives']/nav/div/div/a[1]")).click();
		
		//Get dropdown values for 
		/*List <WebElement> alloptions= driver.findElements(By.xpath("//*[@id='derivatives-equity-dropdown']"));
		for (int i = 0; i < alloptions.size(); i++) {
			String str = alloptions.get(i).getText();
			System.out.println("Option values " + (i + 1) + "    " + str);
		}
		*/
		
		driver.quit();
	}

}
