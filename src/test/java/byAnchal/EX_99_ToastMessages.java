package byAnchal;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EX_99_ToastMessages {

//	private static final Function ExpectedCondition = null;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		
		driver.get("https://demo.actitime.com/");
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id='loginFormContainer']/tbody/tr[1]/td/table/tbody/tr[2]/td/input")).sendKeys("manager");
		driver.findElement(By.xpath("//*[@id='loginButton']/div")).click();
		
		wait.until(ExpectedConditions.titleContains("Login"));		
		System.out.println("Next Page appeared with Title :"+ driver.getTitle());
		
		driver.findElement(By.xpath("//*[@id='topnav']/tbody/tr[1]/td[4]/a")).click();
		
		wait.until(ExpectedConditions.titleContains("Task"));		
		System.out.println("Next Page appeared with Title :"+ driver.getTitle());
		
		driver.findElement(By.xpath("//*[@id='taskManagementPage']/div[1]/div[1]/div[1]/div[1]/div[3]/div/div[2]")).click();
		driver.findElement(By.xpath("/html/body/div[22]/div[1]")).click();
		
		String name ="Ancl";
		driver.findElement(By.xpath("//*[@id='customerLightBox_content']/div[2]/div[1]/div/div[1]/div[1]/input")).sendKeys(name);
		driver.findElement(By.xpath("//*[@id='customerLightBox_content']/div[3]/div[2]/div[1]/div/div[1]")).click();
		
		// NOT ABLE TO GET THE TEXT
		String ToastMessage= driver.findElement(By.xpath("//[@class='toastWrapper']")).getText();
		System.out.println(ToastMessage);
		
/*      WebElement suceesmessage = driver.findElement(By.xpath("//*[@class=\"toastsContainer\"]"));
		wait.until(ExpectedConditions.visibilityOf(suceesmessage));
		String suceesmessage2 = suceesmessage.getText();
		//			String suceesmessage1 = driver.findElement(By.xpath("/html/body/div[13]")).getText();
		System.out.println("Required Toast message is displayed as : " + suceesmessage2);
		//			System.out.println("customer created message" + suceesmessage1);
*/
		
		driver.quit();
		
	}
}
