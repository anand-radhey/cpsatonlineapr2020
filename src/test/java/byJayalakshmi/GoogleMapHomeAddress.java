/**
 * 
 */
package byJayalakshmi;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author Jaya
 *
 */
public class GoogleMapHomeAddress {

	/**
	 * @param args Open http://maps.google.com and write JUnit / WebDriver test for
	 *             the following using Google Chrome:
	 * 
	 *             Search for your home address
	 * 
	 *             Print the address text that is displayed in the left frame
	 * 
	 *             Take screenshot of the page at this instant and save the image
	 * 
	 *             Click on direction and get direction from your office address to
	 *             your home
	 * 
	 *             Display the first option distance and time suggested in eclipse
	 *             console
	 */
	public static void main(String[] args) {
		try {
			// TODO Auto-generated method stub

			WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
			driver.get("http://maps.google.com");
			WebElement seachbox = driver.findElement(By
					.xpath("/html/body/jsl/div[3]/div[9]/div[3]/div[1]/div[1]/div[1]/div[2]/form/div/div[3]/div/input[1]"));
			seachbox.clear();
			seachbox.sendKeys("Abbigere, Bangalore");
			seachbox.sendKeys(Keys.ENTER);
			// *[@id="pane"]/div/div[1]/div/div/div[2]/div/h2
			List<WebElement> alladdress = driver
					.findElements(By.xpath("//*[@id=\"pane\"]/div/div[1]/div/div/div[2]/div/h2"));

			for (WebElement address : alladdress) {

				System.out.println("address is : " + address.getText());
			}

			utils.HelperFunctions.captureScreenShot(driver, "src\\test\\resources\\screenshots\\screen1.jpg");

			driver.findElement(By.xpath("//*[@id=\"pane\"]/div/div[1]/div/div/div[5]/div[1]/div/button/img")).click();

			WebElement officeAddress = driver.findElement(By.xpath("//*[@id=\"sb_ifc51\"]/input"));
			officeAddress.clear();
			officeAddress.sendKeys("Infogain, 7, Koramangala");
			officeAddress.sendKeys(Keys.ENTER);

			String distance = driver
					.findElement(By.xpath("//*[@id=\"section-directions-trip-0\"]/div[2]/div[1]/div[1]/div[2]/div"))
					.getText();
			String time = driver
					.findElement(By.xpath("//*[@id=\"section-directions-trip-0\"]/div[2]/div[1]/div[1]/div[1]/span[1]"))
					.getText();

			System.out.println("First option suggested distance to office is : " + distance + " and suggested time taken is:  " + time);
			driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
