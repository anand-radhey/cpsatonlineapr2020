/**
 * 
 */
package byJayalakshmi;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Jaya
 *
 */
public class WikipediaNavigation {

	/**
	 * @param args Ex01A : WiKiPedia_Navigation
	 * 
	 *             1. Open WiKiPedia.org (Consider it Page1) 2. Click on English (It
	 *             moves to Page2) 3. Write "Selenium" in Search Box 4. Click on
	 *             Magnifying Glass (It moves to Page 3)
	 * 
	 *             5a) try to use driver navigation to goback and print the title of
	 *             the previous page
	 * 
	 *             5b) try to use driver navigation to gofront and print the title
	 *             of the page
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			String url = "https://www.wikipedia.org/";
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\driver\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			By byvar = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
			WebElement english = driver.findElement(byvar);
			english.click();

			By byvar1 = By.id("searchInput");
			WebElement searchbox = driver.findElement(byvar1);
			searchbox.sendKeys("Selenium");

			By byvar2 = By.id("searchButton");
			WebElement mglass = driver.findElement(byvar2);
			mglass.click();

			driver.navigate().back();

			String page2title = driver.getTitle();
			System.out.println("page 2 title after moving back is  " + page2title);

			driver.navigate().forward();

			String page3title = driver.getTitle();
			System.out.println("page 3 title after moving forward is  " + page3title);
			driver.quit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
