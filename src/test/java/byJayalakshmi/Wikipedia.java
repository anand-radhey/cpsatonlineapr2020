/**
 * 
 */
package byJayalakshmi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Jaya
 *
 */
public class Wikipedia {

	/**
	 * @param args Ex01 : WiKi Pedia Search
	 * 
	 *             1. Open WiKiPedia.org (Consider it Page1) 2. Click on English 3.
	 *             Write "Selenium" in Search Box 4. Click on Magnifying Glass 5.
	 *             Print the title. 6. Verify if the Title is "Selenium -
	 *             Wikipedia".
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			String url = "https://www.wikipedia.org/";
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\driver\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get(url);
			driver.manage().window().maximize();
			By byvar = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
			WebElement english = driver.findElement(byvar);
			english.click();

			By byvar1 = By.id("searchInput");
			WebElement searchbox = driver.findElement(byvar1);
			searchbox.sendKeys("Selenium");

			By byvar2 = By.id("searchButton");
			WebElement mglass = driver.findElement(byvar2);
			mglass.click();

			String actualpagetitle = driver.getTitle();
			String expectedTitle = "Selenium - Wikipedia";
			System.out.println("page title is  " + actualpagetitle);

			if (actualpagetitle.equalsIgnoreCase(expectedTitle)) {
				System.out.println("page title is displayed as expected " + expectedTitle);
			} else

				System.out.println("page title is not displayed as " + expectedTitle);
			driver.quit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
