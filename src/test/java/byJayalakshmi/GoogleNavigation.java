/**
 * 
 */
package byJayalakshmi;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Jaya
 *
 */
public class GoogleNavigation {

	/**
	 * @param args Ex02A : Exercise on navigation Print Page Title for each of the
	 *             page below: 1. Go to https://www.google.com/ (Consider it Page1)
	 * 
	 *             2. Search for "seleniumhq.org documentation" (It moves to Page2)
	 * 
	 *             3. click on the first link (It moves to Page 3)
	 * 
	 *             3a) try to use driver navigation to goback and print the title of
	 *             the previous page
	 * 
	 *             3b) try to use driver navigation to gofront and print the title
	 *             of the page
	 * 
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\driver\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			// page 1
			driver.get("https://www.google.com/");

			driver.manage().window().maximize();

			By byvar = By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input");
			WebElement serachbox = driver.findElement(byvar);
			serachbox.click();

			// page 2
			serachbox.sendKeys("seleniumhq.org documentation");
			serachbox.sendKeys(Keys.ENTER);

			// page 3

			driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/a/h3")).click();

			// print page 2 title
			driver.navigate().back();
			System.out.println("page title of 2 page is " + driver.getTitle());

			// print page 3 title
			driver.navigate().forward();
			System.out.println("page title of 3 page is " + driver.getTitle());

			driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
