/**
 * 
 */
package byJayalakshmi;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Jaya
 *
 */
public class Toast {

	/**
	 * @param args
	 * 
	 *             1. Login to https://demo.actitime.com/ 2. Enter login credentials
	 *             - admin, Manager 3. Click on tasks tab on top 4. Click on Add New
	 *             5. Click on New Customer 6. Enter information 7. Click on Create
	 *             Customer buttone 8. Print Toast Message
	 */
	public static void main(String[] args) {
		try {
			// TODO Auto-generated method stub

			WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
			driver.get("https://demo.actitime.com/");
			WebDriverWait wait = new WebDriverWait(driver, 20);

			driver.findElement(By.xpath("//*[@id=\"loginFormContainer\"]/tbody/tr[1]/td/table/tbody/tr[1]/td/input"))
			.sendKeys("admin");
			driver.findElement(By.xpath("//*[@id=\"loginFormContainer\"]/tbody/tr[1]/td/table/tbody/tr[2]/td/input"))
			.sendKeys("manager");
			driver.findElement(By.xpath("//*[@id=\"loginButton\"]/div")).click();

			driver.findElement(By.xpath("//*[@id=\"topnav\"]/tbody/tr[1]/td[4]/a")).click();

			driver.findElement(
					By.xpath("//*[@id=\"taskManagementPage\"]/div[1]/div[1]/div[1]/div[1]/div[3]/div/div[2]")).click();

			driver.findElement(By.xpath("/html/body/div[22]/div[1]")).click();

			// enter customer details -
			//			Please change this name as for each execution as the input name should be unique to create Customer

			driver.findElement(By.xpath("//*[@id=\"customerLightBox_content\"]/div[2]/div[1]/div/div[1]/div[1]/input"))
			.sendKeys("anandtest12");

			WebElement cretebutton = driver
					.findElement(By.xpath("//*[@id=\"customerLightBox_content\"]/div[3]/div[2]/div[1]/div"));
			cretebutton.sendKeys(Keys.PAGE_DOWN);
			cretebutton.click();
			WebElement suceesmessage = driver.findElement(By.xpath("//*[@class=\"toastsContainer\"]"));
			wait.until(ExpectedConditions.visibilityOf(suceesmessage));
			String suceesmessage2 = suceesmessage.getText();
			//			String suceesmessage1 = driver.findElement(By.xpath("/html/body/div[13]")).getText();
			System.out.println("Required Toast message is displayed as : " + suceesmessage2);
			//			System.out.println("customer created message" + suceesmessage1);

			 driver.quit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
