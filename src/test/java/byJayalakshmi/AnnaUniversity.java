/**
 * 
 */
package byJayalakshmi;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * @author Jaya
 *
 */
public class AnnaUniversity {

	/**
	 * @param args Ex03 : Exercise on Anna University
	 * 
	 *             1. Go to http://www.annauniv.edu/department/index.php
	 * 
	 *             2. Hover on Civil Engineering 3. Click on Institute of ocean
	 *             management 4. Print the title
	 */
	public static void main(String[] args) {
		try {
			// TODO Auto-generated method stub

			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\driver\\chromedriver.exe");

			WebDriver driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.get("http://www.annauniv.edu/department/index.php");
			driver.manage().window().maximize();
			//			WebDriverWait wait = new WebDriverWait(driver, 20);

			Actions builder = new Actions(driver);
			// By byvar = By.xpath("//*[@id='menuItemText32']");
			By byvar = By.xpath("//*[@id='link3']/strong");
			WebElement menu = driver.findElement(byvar);
			// menu.click();

			builder.moveToElement(menu).perform();
			driver.findElement(By.xpath("//*[@id=\"menuItemText32\"]")).click();

			//			wait.until(ExpectedConditions.titleContains("Ocean Management"));

			System.out.println("page title is " + driver.getTitle());
			driver.quit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
