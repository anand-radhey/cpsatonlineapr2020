/**
 * 
 */
package byJayalakshmi;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Jaya
 *
 */
public class AlertsHandling {

	/**
	 * @param args Ex04 : Exercise on Alerts Handling (TestNG) 1. Go to
	 *             "https://the-internet.herokuapp.com/javascript_alerts"
	 * 
	 *             2a. Click on JS Alert and Click ok button on Alert Validate
	 *             Result: "You successfully clicked an alert"
	 * 
	 *             2b. Click for JS Confirm i) click Ok and validate Result ii)click
	 *             cancel and validate result
	 * 
	 *             2c. Click for JS Prompt and then i) Input your name and click ok
	 *             to validate result ii) Input your name and click cancel to
	 *             validate result iii) No Input on JS Prompt and click ok to
	 *             validate result iv) No input on JS Prompt and click cancel to
	 *             validate result
	 */
	public static void main(String[] args) {
		try {
			// TODO Auto-generated method stub
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

			WebDriver driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.get("https://the-internet.herokuapp.com/javascript_alerts");
			driver.manage().window().maximize();
			
			WebDriverWait wait = new WebDriverWait(driver, 15);
			
			
			//WebDriverWait wait = new WebDriverWait(driver, 20);

			// 2a exercise
			//*[@id="content"]/div/ul/li[1]/button
//			By byvar = By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button");
//			WebElement jsbutton = driver.findElement(byvar);
//			jsbutton.click();
			
			driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button")).click();
			
			wait.until(ExpectedConditions.alertIsPresent());
			
			 Alert alert = driver.switchTo().alert();
			 
			 alert.accept();
			

			//driver.switchTo().alert().accept();

			String expectedResult = "You successfuly clicked an alert";

			By byvar1 = By.xpath("//*[@id=\"result\"]");
			WebElement alertmessage = driver.findElement(byvar1);

			String actualResult = alertmessage.getText();

			System.out.println(actualResult);

			if (expectedResult.equalsIgnoreCase(actualResult)) {

				System.out.println("result message is displayed properly");
			} else
				System.out.println("Result message is not displayed properly");

			// 2b. Click for JS Confirm i) click Ok and validate Result ii)click
			// * cancel and validate result

			By byvar3 = By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button");
			WebElement jsconfirm = driver.findElement(byvar3);
			jsconfirm.click();
			driver.switchTo().alert().accept();

			String message = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();

			if (message.contains("Ok")) {

				System.out.println("you clicked on OK");
			} else
				System.out.println("Alert message not displayed properly");
			wait.until(ExpectedConditions.titleContains("The Internet"));
			jsconfirm.click();
			driver.switchTo().alert().dismiss();

			String message1 = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
			if (message1.contains("Cancel")) {

				System.out.println("you clicked on Cancel");
			} else
				System.out.println("Alert message not displayed properly");
			/*
			 * 2c. Click for JS Prompt and then i) Input your name and click ok to validate
			 * result ii) Input your name and click cancel to validate result iii) No Input
			 * on JS Prompt and click ok to validate result iv) No input on JS Prompt and
			 * click cancel to validate result
			 */

			By byvar4 = By.xpath("//*[@id=\"content\"]/div/ul/li[3]/button");
			WebElement jsprompt = driver.findElement(byvar4);
			jsprompt.click();
			String name = "JAYALAKSHMI";
			driver.switchTo().alert().sendKeys(name);
			driver.switchTo().alert().accept();

			String input = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();

			if (input.contains(name)) {

				System.out.println("Alert message displayed properly after entering your name and clicking on OK");
			} else
				System.out.println("Alert message not displayed properly after entering your name and clicking on OK");

			wait.until(ExpectedConditions.titleContains("The Internet"));
			jsprompt.click();
			driver.switchTo().alert().sendKeys(name);
			driver.switchTo().alert().dismiss();

			String canelmessage = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
			if (canelmessage.contains("null")) {
				System.out.println("alert message displayed properly after entering your name and clicking on cancel");
			} else
				System.out.println(
						"Alert message not displayed properly after entering your name and clicking on cancel");

			driver.quit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
