package byJayalakshmi;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowSwitching {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//		WebDriverWait wait = new WebDriverWait(driver, 20);
		driver.get("https://www.ataevents.org/");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		By byvar = By.xpath("//*/a[contains(text(),'Improving quality')]");
		WebElement firstlink = driver.findElement(byvar);
		// firstlink.click();

		String parentWindow = driver.getWindowHandle();

		firstlink.click();

		Set<String> allwindows = driver.getWindowHandles();

		Iterator<String> count = allwindows.iterator();

		while (count.hasNext()) {

			String secondWindow = count.next();

			if (!secondWindow.equalsIgnoreCase(parentWindow)) {

				driver.switchTo().window(secondWindow);
				JavascriptExecutor js = (JavascriptExecutor) driver;

				// js.executeScript("scroll(0,400)");

				By byvar1 = By.xpath(
						"//app-event-page/div/div[1]/div/div/div[2]/div[3]/app-event-header/div/div/div/div[3]/div[1]/span[1]");

				System.out.println("page title of 2 page is " + driver.getTitle());

				WebElement date = driver.findElement(byvar1);

				System.out.println("date and time of the event is " + date.getText());

				break;
			}
		}
		
		driver.quit();
		// driver.switchTo().window(parentWindow);

		// By byvar1 =
		// By.xpath("//*[@id=\"main-home-content\"]//div[1]/div[1]/div/div/div[2]/div/p");

	}

}
