package day1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Google {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		
		
System.setProperty("webdriver.gecko.driver","src\\test\\resources\\drivers\\geckodriver.exe");
System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");

  System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"C:\\temp\\logs.txt");
 
		WebDriver driver = new FirefoxDriver();
		
		
		driver.get("http://www.google.com");
		
		String title = driver.getTitle();
		System.out.println("title of the page is : "+ title);
		//Inheritance    
		// Abstract classes/methods
		// Interfaces
		// Polymorphism
		
		//driver.close();
		driver.quit();
		

	}

}
