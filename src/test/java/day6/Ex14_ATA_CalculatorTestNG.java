package day6;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex14_ATA_CalculatorTestNG {

	WebDriver driver;

	@BeforeTest



	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.get("http://ata123456789123456789.appspot.com/");

	}

	@Test
	public void f() {
		
		//*[@id="ID_nameField1"]
		//*[@id="ID_nameField2"]
		//*[@id="gwt-uid-2"]
		//*[@id="ID_calculator"]
		//*[@id="ID_nameField3"]
		
		By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
		By byField2 = By.xpath("//*[@id=\"ID_nameField2\"]");
		By byMul = By.xpath("//*[@id=\"gwt-uid-2\"]");
		By byCalc = By.xpath("//*[@id=\"ID_calculator\"]");
		By byResult = By.xpath("//*[@id=\"ID_nameField3\"]");
		
		WebElement weField1 = driver.findElement(byField1);
		WebElement weField2 = driver.findElement(byField2);
		WebElement weMul = driver.findElement(byMul);
		WebElement weCalc = driver.findElement(byCalc);
		WebElement weResult = driver.findElement(byResult);
		
		
		int iInput1 = 12;
		int iInput2 = 12;

		String input1 = String.valueOf(iInput1);
		String input2 = String.valueOf(iInput2);
		
		

		int expectedResult = 143;
		
		weField1.clear();
		weField1.sendKeys(input1);
		
		weField2.clear();
		weField2.sendKeys(input2);
		weMul.click();
		weCalc.click();
		
		String strActualResult = weResult.getAttribute("value");
		
		int iActualResult = Integer.parseInt(strActualResult);
		
		System.out.println("actual result = " + iActualResult);
		System.out.println("expected result = " + expectedResult);
		
		Assert.assertEquals(iActualResult, expectedResult, "The results do not match");
		
		
	}


	@AfterTest
	public void afterTest() {
		
		driver.quit();
	}

}
