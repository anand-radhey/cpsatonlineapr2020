package day6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import utils.HelperFunctions;

public class Ex14A_ATA_PageFactoryUser {

	WebDriver driver;

	@BeforeTest

	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		driver.get("http://ata123456789123456789.appspot.com/");

	}

	@Test
	public void f() {
		
		EX14A_ATA_PageFactory ataPF = new EX14A_ATA_PageFactory(driver);
		
		int expectedResult = 143;
		String actualResult = ataPF.multiply("12", "12");
		int iActualResult = Integer.parseInt(actualResult);
		Assert.assertEquals(iActualResult, expectedResult,"Results do not match");
		
		
		//Add
		int expectedResultAdd = 24;
		String actualResultAdd = ataPF.Add("11", "13");
		int iActualResultAdd = Integer.parseInt(actualResultAdd);
		Assert.assertEquals(iActualResultAdd, expectedResultAdd,"Results of Addition do not match");

		//Comp
		int expectedResultComp = 12;
		String actualResultComp = ataPF.Comp("12", "11");
		int iActualResultComp = Integer.parseInt(actualResultComp);
		Assert.assertEquals(iActualResultComp, expectedResultComp,"Results of Comparison do not match");

		
		
		
		
	}

	@AfterTest
	public void afterTest() {
		
		driver.quit();
	}
}
