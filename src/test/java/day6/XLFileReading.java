package day6;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class XLFileReading {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		String[][]   mydata =	utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\imdbdata.xlsx", "data");
		int nrows = mydata.length;
		int ncols = mydata[0].length;
		for(int i=0; i < nrows ; i++) {
			for(int j=0; j < ncols ; j++) {
				System.out.print(mydata[i][j] + "\t");

			}
			System.out.println();			
		}

	}

}
