package day3;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex02B_WindowSwitching {

	public static void main(String[] args) {
		
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome", true);
		
		
		
	/*	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		driver.get("https://www.ataevents.org/");
		
		System.out.println("1 . Win Handle " + driver.getWindowHandle());
		String title1 = driver.getTitle();
		
		System.out.println("Title page1 : " + title1);  //Page 1
		
		//*[@id="main-home-content"]/div[3]/div/div/div/div[1]/div[1]/div/div/div[1]/figure/a
		By byvar = By.xpath("//*[@id=\"main-home-content\"]/div[3]/div/div/div/div[1]/div[1]/div/div/div[1]/figure/a");
		WebElement weimage = driver.findElement(byvar);
		
		weimage.click();
		
		System.out.println("2. Win Handle " + driver.getWindowHandle());
		
		String title2 = driver.getTitle();
		System.out.println("Title page2 : " + title2);  //Page 1
		
		
		//image link 2
		//*[@id="main-home-content"]/div[3]/div/div/div/div[1]/div[2]/div/div/div[1]/figure/a
		
		driver.findElement(By.xpath("//*[@id=\"main-home-content\"]/div[3]/div/div/div/div[1]/div[2]/div/div/div[1]/figure/a")).click();
		
		System.out.println("3. Win Handle " + driver.getWindowHandle());
		String title3 = driver.getTitle();
		System.out.println("Title page3 : " + title3);  //Page 1
		
		
		//image link 3
		//*[@id="main-home-content"]/div[3]/div/div/div/div[1]/div[3]/div/div/div[1]/figure/a
		
		driver.findElement(By.xpath("//*[@id=\"main-home-content\"]/div[3]/div/div/div/div[1]/div[3]/div/div/div[1]/figure/a")).click();
		System.out.println("4. Win Handle " + driver.getWindowHandle());
		
		String title4 = driver.getTitle();
		System.out.println("Title page4 : " + title4);  //Page 1
		
		
		driver.findElement(By.xpath("//*[@id=\"main-home-content\"]/div[3]/div/div/div/div[1]/div[4]/div/div/div[1]/figure/a/img")).click();
		
		
	Set<String>    winhandles = driver.getWindowHandles();
	
	
	//{e1, e2,e3,e4   }
	
		for( String strhandle     : winhandles) {
			System.out.println("Window Handle : " +  strhandle);
			driver.switchTo().window(strhandle);
			String title5 = driver.getTitle();
			String expTitle = "Virtual Meetup";
			if(title5.contains(expTitle)) {
				break;
				
			}
			System.out.println("Title of selected Window is  : " + title5);  //Page 1
			
		}
		
		System.out.println("out side for loop and Title of selected Window is  : " + driver.getTitle());  //Page 1
		
		
		utils.HelperFunctions.captureScreenShot(driver,  "src//test//resources//screenshots//screen7headless.jpg");
		
		
		
		
		
		
		
		//driver.close();

	}

}
