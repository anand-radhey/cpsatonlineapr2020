package day3;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex02A_Navigation {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.google.com/");
		
		System.out.println("Title page1 : " + driver.getTitle());  //Google
		//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input
		
		//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input
		
		By byvar1 = By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input") ;
		WebElement obj1 = driver.findElement(byvar1);
		
		obj1.sendKeys("seleniumhq.org documentation");
		obj1.sendKeys(Keys.ENTER);
		
		System.out.println("Title page2 : " + driver.getTitle()); //Search Result Page 2
		
		//*[@id="rso"]/div[1]/div/div[1]/a/h3
		
		//driver.findElement(By.xpath("   ")).senkeys("");

		
		//WebElement welink = driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/a/h3"));
		
		driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/a/h3")).click();
		
		System.out.println("Title page3 : " + driver.getTitle()); //Sel Doc  Page 3
		
		
		driver.navigate().back();
		
		System.out.println("Back from page3 --  Title page : " + driver.getTitle()); //Sel Doc  Page 3
		
		driver.navigate().forward();
		System.out.println("Forward  from page2 --  Title page : " + driver.getTitle()); //Sel Doc  Page 3
		
		
		
		
		
		
		
		
		
		
		
		
		driver.quit();

	}

}
