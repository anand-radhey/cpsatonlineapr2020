package day5;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Ex03_Annauniv_JU.class, Ex09_SampleDropDown_JU.class })
public class AllTests {

}
