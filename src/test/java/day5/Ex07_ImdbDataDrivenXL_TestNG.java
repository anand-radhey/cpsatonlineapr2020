package day5;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Ex07_ImdbDataDrivenXL_TestNG {
	
	WebDriver driver;
	@BeforeTest

	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}	
	
	 @DataProvider
	  public Object[][] dp() throws IOException, EncryptedDocumentException, InvalidFormatException {
		  //String[][] getExcelDataUsingPoi(String fileName, String sheetName);
		String[][] datafromxl = utils.XLDataReaders.getExcelData("src/test/resources/data/imdbdata.xlsx", "data");  
		return datafromxl;  
		/*  
	    return new Object[][] {
	      new Object[] { "Raazi", "Meghna Gulzar", "Vicky Kaushal" },
	      new Object[] { "Uri", "Aditya Dhar", "Vicky Kaushal" },
	    };
	  }
	  */
		
	  }
	
  @Test(dataProvider = "dp")
  public void f(String v1, String v2, String v3) {
		  
		String movieName = v1;
		String expectedDirectorName = v2;
		String expectedStarName = v3;

		driver.get("https://www.imdb.com/");

		//*[@id="navbar-query"]
		By byVar = By.xpath("//*[@id=\"navbar-query\"]");
		WebElement weText = driver.findElement(byVar);
		weText.clear();
		weText.sendKeys(movieName);
		WebDriverWait wait = new WebDriverWait(driver,10);
		weText.sendKeys(Keys.ENTER);

		// //*[@id='main']/div/div[2]/table/tbody/tr[1]/td[2]/a
//		By moviename = By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a");
		
		By byMovieLink = By.partialLinkText(movieName);

		wait.until(ExpectedConditions.elementToBeClickable(byMovieLink));

		WebElement we = driver.findElement(byMovieLink);
		//we.getAttribute("href");
		we.click();

		//*[@id="title-overview-widget"]/div[2]/div[1]/div[2]/a
		//String director = driver.findElement(By.xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[2]/a")).getText();

		//*[@class='inline' and contains(text(),"Director")]/following-sibling::*
		
		By byAllDirectors = By.xpath("//*[@class='inline' and contains(text(),\"Director\")]/following-sibling::*");

		List<WebElement> listOfDirectors = driver.findElements(byAllDirectors);
		
		boolean directorFound = false;
		
		for (int i=0;i<listOfDirectors.size();i++) {
			String directorName = listOfDirectors.get(i).getText();
			//expectedDirectorName
			if (directorName.contains(expectedDirectorName)) {
				directorFound = true;
				break;
			}
		}

		System.out.println("Director " + expectedDirectorName + " Found =" + directorFound);

		
		//*[@class='inline' and text()="Stars:"]/following-sibling::*
		By byAllStars = By.xpath("//*[@class='inline' and text()=\"Stars:\"]/following-sibling::*");

		List<WebElement> listOfStars = driver.findElements(byAllStars);
		
		boolean starFound = false;
		
		for (int i=0;i<listOfStars.size();i++) {
			String starName = listOfStars.get(i).getText();
			System.out.println("Star Names : " + i + starName );
			//expectedDirectorName
			if (starName.contains(expectedStarName)) {
				starFound = true;
				break;
			}
		}

		System.out.println("Star Name " + expectedStarName + " Found =" + starFound);		  
  }

 
 

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}