package day5;



import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Ex03_Annauniv_JU {
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		
		
		System.setProperty("webdriver.chrome.driver","src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}

	@After
	public void tearDown() throws Exception {
		
		driver.quit();
		
	}

	@Test
	public void test() throws InterruptedException {
		//fail("Not yet implemented");
driver.get("http://www.annauniv.edu/department/index.php");
		
		//Civil
		//*[@id="link3"]/strong
		WebElement wecivil = driver.findElement(By.xpath("//*[@id=\"link3\"]/strong"));
		
		//IOM
		//*[@id="menuItemHilite32"]
		WebElement weiom = driver.findElement(By.xpath("//*[@id=\"menuItemHilite32\"]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(wecivil).moveToElement(weiom).click().build().perform();
		System.out.println("Title for IOM page "+ driver.getTitle());
		Thread.sleep(3000);

	}

}
