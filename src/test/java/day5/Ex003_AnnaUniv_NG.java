package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Ex003_AnnaUniv_NG {
	WebDriver driver = null;
	
	@BeforeTest
	  public void beforeTest() {

			System.setProperty("webdriver.chrome.driver","src\\test\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
		  
	  }
	
	
  @Test
  public void f() throws InterruptedException {
	  driver.get("http://www.annauniv.edu/department/index.php");
		
		//Civil
		//*[@id="link3"]/strong
		WebElement wecivil = driver.findElement(By.xpath("//*[@id=\"link3\"]/strong"));
		
		//IOM
		//*[@id="menuItemHilite32"]
		WebElement weiom = driver.findElement(By.xpath("//*[@id=\"menuItemHilite32\"]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(wecivil).moveToElement(weiom).click().build().perform();
		System.out.println("Title for IOM page "+ driver.getTitle());
		Thread.sleep(3000);

	}

	  
  
  

  @AfterTest
  public void afterTest() {
	  
	  driver.quit();
  }

}
