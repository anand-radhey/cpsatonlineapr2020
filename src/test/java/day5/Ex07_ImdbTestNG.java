package day5;



import org.testng.annotations.Test;



import utils.HelperFunctions;



import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;

import org.openqa.selenium.Keys;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.annotations.AfterTest;



public class Ex07_ImdbTestNG {

	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	
	 @DataProvider
	  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		 
			String[][]   mydata =	utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\imdbdata.xlsx", "data");
		  return mydata;
			
		 /*
	    return new Object[][] {
	      new Object[] { "Raazi", "Meghna Gulzar", "Vicky Kaushal", "4th" },
	      new Object[] { "Uri", "Aditya Dhar", "Vicky Kaushal", "5th" },
	  
	    };*/
		  
		  
	  }
	
	
	

	  @Test(dataProvider = "dp")
	  public void f(String v1, String v2, String v3) {

		String movieName = v1 ;    //"Baazigar";
		String expectedDirectorName = v2 ;   // "Mastan";
		String expectedStarName = v3;   //"Shah Rukh Khan";

		driver.get("https://www.imdb.com/");
		
		//*[@id="suggestion-search"]
		
		WebElement weText = driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]"));
		

		weText.clear();
		weText.sendKeys(movieName);
		WebDriverWait wait = new WebDriverWait(driver,10);
		weText.sendKeys(Keys.ENTER);
		
		
		//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a
		
		By byMovieLink = By.partialLinkText(movieName);
		
		wait.until(ExpectedConditions.elementToBeClickable(byMovieLink));
		
		
		WebElement we = driver.findElement(byMovieLink);
		//we.getAttribute("href");
		we.click();
		
		
		//*[@class='inline' and contains(text(),"Director")]/following-sibling::*
		
		By byAllDirectors = By.xpath("//*[@class='inline' and contains(text(),\"Director\")]/following-sibling::*");

		List<WebElement> listOfDirectors = driver.findElements(byAllDirectors);

		boolean directorFound = false;
		for (int i=0;i<listOfDirectors.size();i++) {
			String directorName = listOfDirectors.get(i).getText();
			//expectedDirectorName
			if (directorName.contains(expectedDirectorName)) {
				directorFound = true;
				break;
			}
		}
		
		System.out.println("Director " + expectedDirectorName + " Found =" + directorFound);


	}





	@AfterTest
	public void afterTest() {



		driver.quit();

	}



}