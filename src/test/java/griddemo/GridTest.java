package griddemo;

import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class GridTest {

	static WebDriver driver = null;

	@Parameters({"browser","port"})

	@BeforeMethod 
	public void beforeTest(String browser, String port)
	{
		//compares the value of parameter name with Firefox, if its firefox then it will lauch firefox and run the script.

		if (browser.equalsIgnoreCase("firefox"))
		{
			//driver = new FirefoxDriver();
			//    DesiredCapabilities.firefox();
			/*DesiredCapabilities capabilities = DesiredCapabilities.firefox();
					capabilities.setBrowserName("firefox");
					capabilities.setPlatform(Platform.WINDOWS);
					capabilities.setVersion("ANY");*/
			FirefoxOptions options = new FirefoxOptions();
			options.setCapability(CapabilityType.PLATFORM_NAME, Platform.WINDOWS);
			options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			try {
				driver=new RemoteWebDriver(new URL(port.concat("/wd/hub")),options);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*try {
					driver=new RemoteWebDriver(new URL(port),capabilities);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/

		} else if (browser.equalsIgnoreCase("chrome"))
		{
			//System.setProperty("webdriver.chrome.driver","D:\\Selenium_Training\\chromedriver_win32\\chromedriver.exe");
			//driver= new ChromeDriver(); 
			ChromeOptions options = new ChromeOptions();
			options.setCapability(CapabilityType.PLATFORM_NAME, Platform.WINDOWS);
			options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			try {
				driver=new RemoteWebDriver(new URL(port.concat("/wd/hub")),options);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*try {
						driver=new RemoteWebDriver(new URL(port),capabilities);
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
		}
		else
		{
			throw new IllegalArgumentException("The Browser Type is Undefined");
		}
		driver.get("https://www.wikipedia.org");
	}


	@Test
	public void wiki() throws InterruptedException
	{
		driver.manage().window().maximize();
		//Thread.sleep(5000);


		driver.findElement(By.id("js-link-box-en")).click();
		//Thread.sleep(5000);
		String expectedTitle = "Wikipedia, the free encyclopedia";

		//	fetch the title of the web page and save it into a string variable
		String actualTitle = driver.getTitle();

		Assert.assertEquals(expectedTitle,actualTitle);

		//Thread.sleep(20000);
	}

	@AfterMethod //this annotation would run once test script execution would complete
	public void afterTest() throws InterruptedException
	{
		Thread.sleep(4000);
		driver.quit();
	}
	/*	       try {
		            driver.wait(5000);
		            }
		catch (Exception e){
		             driver.quit();
		           }
		      }*/
}
