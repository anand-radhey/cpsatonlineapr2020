/* Ex06 : ATASocialMediaHrefJunit (JUnit and TestNG)
1. Go to "http://agiletestingalliance.org/"
2. print hrefs for following social media icons at the footer
- LinkedIn
- twitter
- youtube
- insta
- facebook
- wa
- Telegram
 */

package day4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class EX06_ATASocialMediaHref_Anchal {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("http://agiletestingalliance.org/");
		String link1 = driver.findElement(By.xpath("//a[@title='LinkedIn']")).getAttribute("href");
      	System.out.println(link1);
		driver.quit();
		

	}

}
