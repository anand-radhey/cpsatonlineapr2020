package day4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniv {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		
		driver.get("http://www.annauniv.edu/department/index.php");
		
		//Civil
		//*[@id="link3"]/strong
		WebElement wecivil = driver.findElement(By.xpath("//*[@id=\"link3\"]/strong"));
		
		//IOM
		//*[@id="menuItemHilite32"]
		WebElement weiom = driver.findElement(By.xpath("//*[@id=\"menuItemHilite32\"]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(wecivil).moveToElement(weiom).click().build().perform();
		System.out.println("Title for IOM page "+ driver.getTitle());
		
		driver.quit();
		

	}

}
