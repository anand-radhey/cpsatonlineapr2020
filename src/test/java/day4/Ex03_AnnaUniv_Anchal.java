/* Ex03 : Exercise on Anna University
1. Go to http://www.annauniv.edu/department/index.php
2. Hover on Civil Engineering
3. Click on Institute of ocean management
4. Print the title
 */
package day4;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniv_Anchal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "C:/Users/Hira/workspace-Neon/mavenbootcampjava/src/test/resources/drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.annauniv.edu/department/index.php");
		
		WebElement ele =driver.findElement(By.xpath("//strong[contains(text(),' Faculty of Civil Engineering')]"));
		Actions action= new Actions(driver);
		//action.moveToElement(ele).build().perform();
		WebElement option = driver.findElement(By.xpath("//div[@id='menuItemHilite32']"));
		//action.keyDown(Keys.CONTROL).click(option).build().perform();
		
		action.moveToElement(ele).moveToElement(option).click().build().perform();
		
		System.out.println(driver.getTitle());
		driver.quit(); 

	}

}
