package day4;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
/*1. Go to "http://agiletestingalliance.org/"
2. print hrefs for following social media icons at the footer
- LinkedIn
- twitter
- youtube
- insta
- facebook
- wa
- Telegram*/

public class Ex06_ATASocialHref_Jinendra {
	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("http://agiletestingalliance.org/");

		    //driver.getWindowHandles()

		List<WebElement>   welist  = driver.findElements(By.xpath("//*[@id=\"custom_html-10\"]/div/ul/li[*]/a"));

		for(int i = 0 ; i < welist.size() ; i++) {
			String str = welist.get(i).getAttribute("href");
			System.out.println("Href for icon "+ (i+1) + "    "+ str);

		}


	}
}