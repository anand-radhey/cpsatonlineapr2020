package zref;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniv {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		
		driver.get("http://www.annauniv.edu/department/index.php");
		
		//*[@id="link3"]/strong     //Civil Engg
		
		WebElement wecivil = driver.findElement(By.xpath("//*[@id=\"link3\"]/strong"));
		
		WebElement weioc   = driver.findElement(By.xpath("//*[@id=\"menuItemHilite32\"]"));
		
		Actions action = new Actions(driver);
		
		action.moveToElement(wecivil).moveToElement(weioc).click().build().perform();
		
		System.out.println("The Title of Department of Oceanography Page is :: "+ driver.getTitle());
		
		driver.quit();
	}

}
