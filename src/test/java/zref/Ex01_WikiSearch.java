package zref;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex01_WikiSearch {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.out.println("This is searching Selenium at Wikipedia page");

		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("https://www.wikipedia.org/");

		// //*[@id="js-link-box-en"]/strong

		By byVar1 = By.xpath("//*[@id=\"js-link-box-en\"]/strong");

		WebElement weText = driver.findElement(byVar1);

		weText.click();

		By byVar2 = By.xpath("//*[@id=\"searchInput\"]");
		WebElement weSearchBox = driver.findElement(byVar2);
		weSearchBox.sendKeys("Selenium");

		// //*[@id="searchButton"] 

		By byVar3 = By.xpath("//*[@id=\"searchButton\"]");
		WebElement weSearchGlass = driver.findElement(byVar3);
		weSearchGlass.click();

		/*
		 * By byVar4 = By.xpath("//title");

		WebElement we1 = driver.findElement(byVar4);
		 */

		String PageTitle=driver.getTitle();
		System.out.println("Wikepedia title is :"+PageTitle);
		boolean Result;

		Result=PageTitle.contains("Selenium - Wikipedia");

		if (Result==true) {
			System.out.println("Pass");
		}
		else {
			System.out.println("Fail");
		} 

		Thread.sleep(3000);
		driver.quit();


		//driver.navigate().forward();






	}

}
