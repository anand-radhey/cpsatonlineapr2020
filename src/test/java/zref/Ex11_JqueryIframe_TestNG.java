package zref;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Ex11_JqueryIframe_TestNG {
	WebDriver driver ;
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome",false);

		driver.get("http://jqueryui.com/autocomplete/");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}




	@Test
	public void f() throws InterruptedException {


		// To handle the frame (Using by index)
		driver.switchTo().frame(0);
		WebElement element = driver.findElement(By.className("ui-autocomplete-input"));
		element.sendKeys("j");


		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/ul/li")));


		List<WebElement> lst = driver.findElements(By.xpath("/html/body/ul/li"));
		for(int i=0; i < lst.size(); i++){
			//WebElement ele = driver.findElement(By.xpath("/html/body/ul/li["+i+"]"));
			WebElement ele = lst.get(i);
			System.out.println(ele.getText());
			if(ele.getText().equals("Java")){
				System.out.println(i);
				ele.click();
				Thread.sleep(3000);
			}
		}


	}


	@AfterTest
	public void afterTest() {
		driver.quit();

	}


}
