package zref;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ex04_AlertsHandling_Rama_JU {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		driver.manage().window().maximize();
	}

	@Test
	public void test() throws Exception {

		By clickJSAlert = By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button");
		WebElement JSAlert = driver.findElement(clickJSAlert);
		JSAlert.click();

		driver.switchTo().alert().accept();

		By actual = By.xpath("//*[@id=\"result\"]");
		WebElement actualtext = driver.findElement(actual);
		String actualText = actualtext.getText();

		if (actualText.equals("You successfuly clicked an alert")) {
			System.out.println("click on alert");
		} else {
			System.out.println("not click on alert");
		}


		By clickJSConfirm = By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button");
		WebElement JSConfirm = driver.findElement(clickJSConfirm);
		JSConfirm.click();

		driver.switchTo().alert().dismiss();

		By actual1 = By.xpath("//*[@id=\"result\"]");
		WebElement actualtext1 = driver.findElement(actual1);
		String actualText1 = actualtext.getText();

		if (actualText1.equals("You clicked: Cancel")) {
			System.out.println("You clicked: Cancel");
		} else {
			System.out.println("You clicked: Cancel not clicked");
		}


		By clickJSPrompt = By.xpath("//*[@id=\"content\"]/div/ul/li[3]/button");
		WebElement JSPrompt = driver.findElement(clickJSPrompt);
		JSPrompt.click();


		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.alertIsPresent());

		driver.switchTo().alert().sendKeys("Rama");

		Thread.sleep(5000);
		driver.switchTo().alert().accept();


		String message = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
		if (message.equals("Rama")) {
			System.out.println("Actual and Expected are matched");
		}

		driver.switchTo().alert().dismiss();

		String message1 = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
		System.out.println(message1);

		if (message.equals("null")) {
			System.out.println("No text are entereted");
		}
	}

	@After
	public void tearDown() throws Exception {

		driver.quit();
	}

}