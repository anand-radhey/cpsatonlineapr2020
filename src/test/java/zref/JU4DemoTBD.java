package zref;



import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class JU4DemoTBD {
	WebDriver driver;
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		//fail("Not yet implemented");
		String url = "http://www.annauniv.edu";
		driver.get(url);
		String pageTitle = driver.getTitle();
		System.out.println("The Title of the Page is :: "+ pageTitle);
		driver.findElement(By.xpath("//a[contains(text(),'Departments')]")).click();
		String pageTitle1 = driver.getTitle();
		System.out.println("The Title of Department  Page is :: "+ pageTitle1);
		WebElement element = driver.findElement(By.xpath("//strong[contains(text(),'Faculty of Civil Engineering')]"));
		Actions action = new Actions(driver);
		action.moveToElement(element).moveToElement(driver.findElement(By.xpath("//div[@id='menuItemHilite32']"))).click().build().perform();
		String pageTitle2 = driver.getTitle();
		System.out.println("The Title of Department of Oceanography Page is :: "+ pageTitle2);

		Thread.sleep(3000);
	}

}
