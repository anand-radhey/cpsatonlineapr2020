package zref;

import java.util.*;

public class HashMapDemo {
    public static void main(String[] args) {
        HashMap<Integer,String>  hmap = new HashMap<Integer, String>();
        hmap.put(500,"Brazil");
        hmap.put(21,"India");
        hmap.put(78, "Japan");
        hmap.put(32, "China");
        hmap.put(77, "USA");

        System.out.println(hmap);
        System.out.println("Before sorting ");
        for( Map.Entry       me:hmap.entrySet()){
            System.out.println(me.getKey()+" " + me.getValue());
        }


        Map<Integer, String> smap = new TreeMap<Integer, String>(hmap);

        System.out.println("After sorting ");
        for( Map.Entry       me:smap.entrySet()){
            System.out.println(me.getKey()+" " + me.getValue());
        }




    }
}
