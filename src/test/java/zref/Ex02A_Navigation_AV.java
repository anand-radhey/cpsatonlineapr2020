package zref;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex02A_Navigation_AV {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com/");
		
		System.out.println("Title Page1 : " + driver.getTitle());
		
		//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input
		
		By byvar1 = By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input");
		WebElement weinputbox = driver.findElement(byvar1);
		
		weinputbox.sendKeys("seleniumhq.org documentation");
		
		weinputbox.sendKeys(Keys.ENTER);
		
		System.out.println("Title Page2 : " + driver.getTitle());
		
		//First Link on Google search results page2
		//*[@id="rso"]/div[1]/div/div[1]/a/h3
		
		By byvar2 = By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/a/h3");
		
		WebElement welink = driver.findElement(byvar2);
		welink.click();
		System.out.println("Title of page3  : "+driver.getTitle());

        driver.navigate().back();
        
        String Title1 = driver.getTitle();
        System.out.println("Title of page1: "+Title1);
        driver.navigate().back();
        String Title2 = driver.getTitle();
        System.out.println("Title of page2: "+Title2);
        try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        driver.close();
		
		
		
		
		
		
		
		
		

	}

}
