package zref;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.HelperFunctions;

public class Ex06_SocialMedia_JU {

	WebDriver driver ;
	
	@Before
	public void setUp() throws Exception {
		
		//System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		
		//driver = new ChromeDriver();
		
		driver = HelperFunctions.createAppropriateDriver("chrome",true);
		//driver = HelperFunctions.createAppropriateDriver("fireFox",false);
		
		driver.get("http://agiletestingalliance.org/");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		//fail("Not yet implemented");
		
		By bySocialMedia = By.xpath("/html/body/footer/div/a[*]");
		
		List<WebElement> list = driver.findElements(bySocialMedia);
		
		int size = list.size();
		
		for (int i=0;i<size;i++) {
			
			WebElement element = list.get(i);
			String href = element.getAttribute("href");
			System.out.println(href);
		}
		
		HelperFunctions.captureScreenShot(driver, "src//test//resources//screenshots//filename.jpg");
	}

}