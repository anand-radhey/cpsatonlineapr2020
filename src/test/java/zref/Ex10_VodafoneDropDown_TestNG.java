package zref;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Ex10_VodafoneDropDown_TestNG {

	WebDriver driver ;
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome",false);

		driver.get("https://www.vodafone.in/");
		driver.manage().window().maximize();

		//driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}


	@Test
	public void f() {

		//*[@id="vfid_body"]/div/div[1]/div[1]/div/ul/li[3]/a
		JavascriptExecutor js=(JavascriptExecutor) driver;
		String scroll="window.scrollBy(0,-500)";
		js.executeScript(scroll);
		
		//Thread.sleep(1000);


		
		By byAboutus = By.xpath("//*[@id=\"vfid_body\"]/div/div[1]/div[1]/div/ul/li[3]/a");

		WebDriverWait wait = new WebDriverWait(driver,10);

		wait.until(ExpectedConditions.elementToBeClickable(byAboutus));

		WebElement element = driver.findElement(byAboutus);

		element.click();

		By byselectlocation=By.xpath("//*[@id=\"ctl00_CU_ddlCircle\"]");


		WebElement element1=driver.findElement(byselectlocation);
		Select selectObject = new Select(element1);

		List <WebElement> listOfOptions = selectObject.getOptions();

		for (int i=0;i<listOfOptions.size();i++) {

			System.out.println(listOfOptions.get(i).getText());
		}

		
		selectObject.selectByVisibleText("Gujarat");

		// here the page refreshes
		// we need to refresh the element w.r.t to the new page
		// refresh or get a new select instance w.r.t this element
		
		element1=driver.findElement(byselectlocation);

		selectObject = new Select(element1);

		selectObject.selectByVisibleText("rajasthan");

		element1=driver.findElement(byselectlocation);

		selectObject = new Select(element1);

		selectObject.selectByValue("punjab");


		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}