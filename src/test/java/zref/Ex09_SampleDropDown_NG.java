package zref;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class Ex09_SampleDropDown_NG {

	WebDriver driver ;
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome",false);

		driver.get("file:///C:/ZVA/CPSAT/mavencpsatjun789BLR/src/test/resources/data/dropdown.html");
		//driver.get("src/test/resources/data/dropdown.html");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}


	@Test
	public void f() {

		///html/body/form/select
		// 
		By byDropdown = By.xpath("/html/body/form/select");
		WebElement element = driver.findElement(byDropdown);
		Select selectObject = new Select(element);

		List <WebElement> listOfOptions = selectObject.getOptions();

		for (int i=0;i<listOfOptions.size();i++) {

			System.out.println(listOfOptions.get(i).getText());
		}

		selectObject.selectByVisibleText("CP-AAT");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		selectObject.selectByVisibleText("CP-MLDS");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
