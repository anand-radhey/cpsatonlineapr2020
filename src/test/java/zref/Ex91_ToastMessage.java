package zref;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ex91_ToastMessage {

	public static void main(String[] args) {
		
		String[] arr = {"Success","Warning","Error","Loading"};
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		driver.get("https://www.jqueryscript.net/demo/toast-alert-message-kk/");
		//Success
		//     /html/body/div[1]/button[1]
		List<WebElement> welst = driver.findElements(By.xpath("/html/body/div[1]/button"));
		
		for(int i=0; i < welst.size() ; i++) {
			welst.get(i).click();
			WebElement wetoast = driver.findElement(By.xpath("/html/body/div[4]/div/div/span"));
			wait.until(ExpectedConditions.visibilityOf(wetoast));
			String toastMsg = wetoast.getText();
			System.out.println("Toast Message for Success is :::: "+ toastMsg);
			
			
			utils.HelperFunctions.captureScreenShot(driver, "src//test//resources//screenshots//image_"+arr[i]+".jpg");
			
			wait.until(ExpectedConditions.invisibilityOf(wetoast));
		}
		
	}

}
