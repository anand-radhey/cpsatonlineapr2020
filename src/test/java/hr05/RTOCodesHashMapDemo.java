package hr05;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class RTOCodesHashMapDemo {
    public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException {
        String[][] rtodata =
                utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\RTOCodes_MH.xlsx",
                        "MAH");

        HashMap<String,String> hmap = new HashMap<String, String>();
        for(int i=0; i<rtodata.length; i++){
            String name = rtodata[i][0];
            String code = rtodata[i][1];
            hmap.put(name, code);

        }

       // hmap.entrySet()


        System.out.println("Before sorting ");
        for( Map.Entry       me:hmap.entrySet()){
            System.out.println(me.getKey()+" " + me.getValue());
        }

        Map<String, String> smap = new TreeMap<String, String>(hmap);

        System.out.println("\n\n\nAfter sorting ");
        for( Map.Entry       me:smap.entrySet()){
            System.out.println(me.getKey()+" " + me.getValue());
        }


    }
}
