package hr05;

public class CricketPlayer implements Comparable<CricketPlayer>{

    String team;
    String name;
    int score;

    public CricketPlayer(String team, String name, int score) {
        this.team = team;
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return "CricketPlayer{" +
                "team='" + team + '\'' +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}'+"\n";
    }

	public int compareTo(CricketPlayer obj2) {
		// TODO Auto-generated method stub
		return (obj2.name.compareTo(this.name));//Descending Order of name
		//return 0;
	}


}